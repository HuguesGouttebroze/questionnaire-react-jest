import React from 'react'
import ReactDOM from 'react-dom/client'
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import {Home} from './pages/Home'
import './index.css'
import { Survey } from './pages/Survey'
import { Header } from './components/Header'
import { Error } from './components/Error'
import { Candidats } from './pages/Candidats'
import { Dashboard } from './pages/Dashboard'
import { Results } from './pages/Results/Results'
// import { createGlobalStyle } from 'styled-components'
import { SurveyProvider, ThemeProvider } from './utils/context'
import { Footer } from './components/Footer'
import { GlobalStyle } from './utils/style/GlobalStyle'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Router>
      <ThemeProvider>
        <SurveyProvider>
          <GlobalStyle />
          <Header />
          <Routes>
            <Route path='/' element={<Home />} />
            <Route path='/survey/:questionNumber' element={<Survey />} />
            <Route path='/candidats' element={<Candidats />} />
            <Route path='/dashboard' element={<Dashboard />} />
            <Route path='/results' element={<Results />} />
            <Route path='/*' element={<Error />} />
          </Routes>
          <Footer />
        </SurveyProvider>
      </ThemeProvider>
    </Router>   
  </React.StrictMode>
)
