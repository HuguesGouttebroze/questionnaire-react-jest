/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
import { useContext } from "react";
import { Link } from "react-router-dom";
import { SurveyContext } from "../../utils/context";
import { StyledLink } from '../../utils/style/Atoms'
import { colors } from "../../utils/style/colors";
import styled from 'styled-components'
import {useFetch} from '../../Hooks/useFetch'
import { Loader } from '../../components/Loader'

const ResultsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 60px 90px;
  padding: 30px;
  background-color: ${({ theme }) =>
    theme === 'light' ? colors.backgroundLight : colors.backgroundDark};
`

const ResultsTitle = styled.h2`
  color: ${({ theme }) => (theme === 'light' ? '#000000' : '#ffffff')};
  font-weight: bold;
  font-size: 28px;
  max-width: 60%;
  text-align: center;
  & > span {
    padding-left: 10px;
  }
`

const DescriptionWrapper = styled.div`
  padding: 60px;
`

const JobTitle = styled.span`
  color: ${({ theme }) =>
    theme === 'light' ? colors.primary : colors.backgroundLight};
  text-transform: capitalize;
`

const JobDescription = styled.div`
  font-size: 18px;
  & > p {
    color: ${({ theme }) => (theme === 'light' ? colors.secondary : '#ffffff')};
    margin-block-start: 5px;
  }
  & > span {
    font-size: 20px;
  }
`

const LoaderWrapper = styled.div`
  display: flex;
  justify-content: center;
`

export function formatJobList(title, listLength, index) {
  if (index === listLength - 1) {
    return title
  }
  return `${title}`
}

export function formatQueryParams(answers) {
  const answerNumbers = Object.keys(answers)

  return answerNumbers.reduce((previousParams, answerNumber, index) => {
    const isFirstParam = index === 0
    const separator = isFirstParam ? '' : '&'
    return `${previousParams}${separator}a${answerNumber}=${answers[answerNumber]}`
  }, '')
}

export function Results() {
  const { answers } = useContext(SurveyContext)
  console.log(answers);
  const queryParams = formatQueryParams(answers)
  const {data, isLoading, error} = useFetch(`http://localhost:8000/results?${queryParams}`)

    if (error) {
    return <span>Il y a un problème</span>
  }

  const resultsData = data?.resultsData

  return isLoading ? (
    <LoaderWrapper>
      <Loader />
    </LoaderWrapper> 
  ) : (
    <ResultsContainer>
      <ResultsTitle>
        Les compétences demandées:
        {resultsData && 
          resultsData.map((result, index) => (
            <JobTitle key={`result-title-${index}-${result.title}`}
            >
              {formatJobList(result.title, resultsData.length, index)}
            </JobTitle>
          ))}
        
      </ResultsTitle>
      <StyledLink $isFullLink to='/candidats'>
          Voir nos candidats
      </StyledLink>
      <DescriptionWrapper>
        {resultsData && 
          resultsData.map((result, index) => (
           <JobDescription key={`result-detail-${index}-${result.title}`}
           >
            <JobTitle>{result.title}</JobTitle>
            <p>{result.description}</p>
          </JobDescription> 
          ))}
        
      </DescriptionWrapper>
      <Link to='/'>Retour à l'accueil</Link>
    </ResultsContainer>
      
  )
}
