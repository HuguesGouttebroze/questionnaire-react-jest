/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-unused-vars
import React, { useContext, useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import styled from 'styled-components'
import { colors } from '../utils/style/colors'
import { Loader } from '../components/Loader'
import { SurveyContext } from '../utils/context'
import { useFetch } from '../Hooks/useFetch'

const SurveyContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
const QuestionTitle = styled.h2`
  text-decoration: underline;
  text-decoration-color: ${colors.primary};
`
const QuestionContent = styled.span`
  margin: 30px;
`
const LinkWrapper = styled.div`
  padding-top: 30px;
  & a {
    color: black;
  }
  & a:first-of-type {
    margin-right: 20px;
  }
`
const ReplyBox = styled.button`
  border: none;
  height: 100px;
  width: 300px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${colors.backgroundLight};
  border-radius: 30px;
  cursor: pointer;
  box-shadow: ${(props) =>
    props.isSelected ? `0px 0px 0px 2px ${colors.primary} inset` : 'none'};
  &:first-child {
    margin-right: 15px;
  }
  &:last-of-type {
    margin-left: 15px;
  }
`

const ReplyWrapper = styled.div`
  display: flex;
  flex-direction: row;
`

export function Survey() {
    const { questionNumber } = useParams()
    const questionNumberInt = parseInt(questionNumber)
    const prevQuestion = questionNumberInt === 1 ? 1 : questionNumberInt - 1
    const nextQuestion = questionNumberInt + 1 

    // const [surveyData, setSurveyData] = useState({})
    // const [isDataLoading, setDataLoading] = useState(false)
    // const [error, setError] = useState(false)
    const { answers, saveAnswers } = useContext(SurveyContext)

    const saveReply = (answer) => {
      saveAnswers({ [questionNumber]: answer })
    }

    const { data, isLoading, error } = useFetch(`http://localhost:8000/survey`)
    const { surveyData } = data

    /* useEffect(() => {
      async function fetchSurvey() {
        setDataLoading(true)
        try {
          const response = await fetch(`http://localhost:8000/survey`)
          const { surveyData } = await response.json()
          setSurveyData(surveyData)
        } catch (err) {
          console.log(err)
          setError(true)
        } finally {
          setDataLoading(false)
        }
      }
      fetchSurvey()
    }, []) */

    /* useEffect(() => {
      fetch(`http://localhost:8000/survey`)
        .then((response) => response.json()
        .then(({ surveyData }) => console.log(surveyData))
        .catch((error) => console.log(error))
      )
    }, []) */

    if (error) {
      return <span>Ai ai ai, y a un probleme ...</span>
    }

  return (
    <SurveyContainer>
      <QuestionTitle>
        Questionnaire 🧮 - question {questionNumber}
      </QuestionTitle>
      {isLoading ? (
        <Loader />
      ) : (
        <QuestionContent>
          {surveyData && surveyData[questionNumber]}  
        </QuestionContent>
      )}
      <ReplyWrapper>
        <ReplyBox 
          onClick={() => saveReply(true)}
          isSelected={answers[questionNumber] === true}>
            oui
        </ReplyBox>
        <ReplyBox 
          onClick={() => saveReply(false)}
          isSelected={answers[questionNumber] === false}>
            non
        </ReplyBox>
      </ReplyWrapper>
      <LinkWrapper>
        <Link to={`/survey/${prevQuestion}`}>Précédent - </Link>
        {questionNumberInt === 10 ? (
          <Link to='/results'>Résultats</Link> 
        ) : ( 
          <Link to={`/survey/${nextQuestion}`}>Suivant</Link>
        )}
      </LinkWrapper>
    </SurveyContainer>
  )      

}
