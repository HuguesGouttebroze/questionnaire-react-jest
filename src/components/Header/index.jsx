import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { colors } from '../../utils/style/colors'

const NavContainer = styled.nav`
  padding: 30px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const StyledLink = styled(Link)`
    padding: 15px;
    color: #8186a0;
    text-decoration: none;
    font-size: 18px;
    ${(props) => 
      props.$isFullLink && 
      `color: white; border-radius: 30px; background-color: ${colors.pimary};`}
`

export function Header() {
  return (
    
    <NavContainer>    
      <StyledLink $isFullLink to='/'>Accueil</StyledLink>
      <StyledLink to='/survey/1'>Questionnaire</StyledLink> 
      <StyledLink to='/informations'>Informations</StyledLink>
      <StyledLink to='/dashboard'>Tableau de bord</StyledLink>
      <StyledLink to='/candidats'>Espace candidats</StyledLink>
    </NavContainer> 
  )
}
